//オンデマンド出力用２面付スクリプト


///配置用ファンクション//
////////////////////////

//中綴じ
function saddle_Left() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    if (r % 2 == 0) { //偶数ページの処理
        //左配置
        app.pdfPlacePreferences.pageNumber = ep - r;
        menL();
        //右配置
        app.pdfPlacePreferences.pageNumber = sp + r; //　ページ数
        menR();
    } else { //奇数ページの処理
        //左配置
        app.pdfPlacePreferences.pageNumber = sp + r;
        menL();
        //右配置
        app.pdfPlacePreferences.pageNumber = ep - r; //　ページ数
        menR();
    }
}

function saddle_Right() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    if (r % 2 == 0) { //偶数ページの処理
        //左配置
        app.pdfPlacePreferences.pageNumber = sp + r;
        menL();
        //右配置
        app.pdfPlacePreferences.pageNumber = ep - r; //　ページ数
        menR();
    } else { //奇数ページの処理
        //左配置
        app.pdfPlacePreferences.pageNumber = ep - r;
        menL();
        //右配置
        app.pdfPlacePreferences.pageNumber = sp + r; //　ページ数
        menR();
    }
}
//平綴じ　左
function side_Left() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    //左配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 3;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp;
    menR();

    //ページを追加
    app.activeDocument.pages.add();
    page_Num++;
    pageObj = app.activeDocument.pages[page_Num];

    //左配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 1;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 2;
    menR();
}
//平綴じ　右
function side_Right() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    //左配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 3;
    menR();

    //ページを追加
    app.activeDocument.pages.add();
    page_Num++;
    pageObj = app.activeDocument.pages[page_Num];

    //左配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 2;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r * 4 + sp + 1;
    menR();
}
//カット&スタック
function cut_stack() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    //左配置
    app.pdfPlacePreferences.pageNumber = r + sp;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r + sp + tp / 2;
    menR();
}
//左右リピート
function repeat() {
    pageObj = app.activeDocument
    pageObj = app.activeDocument.pages[page_Num];
    //左配置
    app.pdfPlacePreferences.pageNumber = r + sp;
    menL();
    //右配置
    app.pdfPlacePreferences.pageNumber = r + sp;
    menR();
}

///////////////////////////////
//オブジェクト作成ファンクション//
///////////////////////////////
function menL() {
    txtObj = pageObj.textFrames.add();
    txtObj.visibleBounds = BoundsValue_L;
    txtObj.place(filename);
    txtObj.fit(FitOptions.centerContent);
    txtObj.visibleBounds = BoundsValue_L_modi;
}

function menR() {
    txtObj = pageObj.textFrames.add();
    txtObj.visibleBounds = BoundsValue_R;
    txtObj.place(filename);
    txtObj.fit(FitOptions.centerContent);
    txtObj.visibleBounds = BoundsValue_R_modi;
}

///////////////////
//ダイアログの表示//
//////////////////

//PDFの読み込み設定
app.pdfPlacePreferences.pdfCrop = PDFCrop.CROP_MEDIA;
//ドキュメントの読み込み
filename = File.openDialog("PDFを選択してください");

var dialog_Obj = app.dialogs.add({name: "PDF_2面付け用"});

//サイズの選択
var size_tmp = dialog_Obj.dialogColumns.add();
var sizeList = size_tmp.dropdowns.add({
    stringList: ["A4", "B5", "カスタム"],
    selectedIndex: 0,
    minWidth: 100
});
//綴じかたの選択
var bind_tmp = dialog_Obj.dialogColumns.add();
var bindList = bind_tmp.dropdowns.add({
    stringList: ["中綴じ_左閉じ", "中綴じ_右閉じ", "平綴じ_左閉じ", "平綴じ_右閉じ", "左右リピート", "カット&スタック"],
    selectedIndex: 0,
    minWidth: 100
});
//開始ページと終了ページ
var s_ptmp = dialog_Obj.dialogColumns.add();
var s_Value = s_ptmp.integerEditboxes.add({
    editValue: 1,
    minimumValue: 1,
    maximumValue: 255
});
var e_ptmp = dialog_Obj.dialogColumns.add();
var e_Value = e_ptmp.integerEditboxes.add({
    editValue: 1,
    minimumValue: 1,
    maximumValue: 255
});
//センタードブの有無
var bleed_tmp = dialog_Obj.dialogColumns.add();
var bleed_check = bleed_tmp.checkboxControls.add({
    staticLabel: "センタードブあり",
    minWidth: 120
});

var flag = dialog_Obj.show();

//ＯＫかキャンセルか
if (flag == true) {

    //////////////////////////////////////
    //ページサイズとオブジェクトサイズの設定//
    //////////////////////////////////////

    var sizeNo = sizeList.selectedIndex;
    var page_Width
    var page_Heigth
    if (sizeNo == 0) {
        page_Width = 210;
        page_Heigth = 297;
    } else if (sizeNo == 1) {
        page_Width = 182;
        page_Heigth = 257;
    } else if (sizeNo == 2) {
        page_Width = parseInt(prompt("ヨコ(mm)", ""));
        page_Heigth = parseInt(prompt("タテ(mm)", ""));
    }

    //サイズ規定用
    var obj_Width = page_Width + 6;
    var obj_Heigth = page_Heigth + 6;
    var doc_Width = page_Width * 2;
    var doc_Heigth = page_Heigth;
    var bleed = 3;
    var start_point = -3;

    //配置用オブジェクト作成用の配列
    if (bleed_check.checkedState == false) {
        var BoundsValue_L = [start_point, start_point, start_point + obj_Heigth, start_point + obj_Width];
        var BoundsValue_R = [start_point, page_Width, start_point + obj_Heigth, start_point + page_Width + obj_Width];
        var BoundsValue_L_modi = [start_point, start_point, start_point + obj_Heigth, start_point + obj_Width - bleed];
        var BoundsValue_R_modi = [start_point, page_Width, start_point + obj_Heigth - bleed, start_point + page_Width + obj_Width];
    } else {
        var BoundsValue_L = [start_point, start_point, start_point + obj_Heigth, start_point + obj_Width];
        var BoundsValue_R = [start_point, page_Width + bleed, start_point + obj_Heigth, bleed + page_Width + obj_Width];
        var BoundsValue_L_modi = [start_point, start_point, start_point + obj_Heigth, start_point + obj_Width];
        var BoundsValue_R_modi = [start_point, page_Width + bleed, start_point + obj_Heigth, bleed + page_Width + obj_Width];
    }



    //ページの作成（ドブチェックありにはトンボを追加）
    pageObj = app.documents.add();
    //単位を初期化_mm
    with(pageObj.viewPreferences){
        horizontalMeasurementUnits = MeasurementUnits.MILLIMETERS;
        verticalMeasurementUnits = MeasurementUnits.MILLIMETERS;
        lineMeasurementUnits = MeasurementUnits.MILLIMETERS;
        textSizeMeasurementUnits = MeasurementUnits.Q;
        typographicMeasurementUnits = MeasurementUnits.HA;       
    }
    if (bleed_check.checkedState == false) {
        with(pageObj.documentPreferences) {
            facingPages =false;
            pageWidth = page_Width * 2;
            pageHeight = page_Heigth;
        }
    } else {
        with(pageObj.documentPreferences) {
            facingPages =false;
            pageWidth = page_Width * 2 + bleed * 2;
            pageHeight = page_Heigth;
            documentSlugUniformSize = true;
            slugTopOffset = "13mm";
        }
        var master = pageObj.masterSpreads.item(0).pages.item(0);
        trimValue = [
            [
                [page_Width, -13],
                [page_Width, -3]
            ],
            [
                [page_Width + 6, -13],
                [page_Width + 6, -3]
            ],
            [
                [page_Width, 300],
                [page_Width, 310]
            ],
            [
                [page_Width + 6, 300],
                [page_Width + 6, 310]
            ]
        ];
        for (i = 0; i <= 3; i++) {
            var trimLine = master.graphicLines.add();
            trimLine.paths[0].entirePath = trimValue[i];
            trimLine.strokeWeight = "0.1mm"
        }


    }



    //綴じ方リストのインデックスを取得
    var bindding = bindList.selectedIndex;

    //ページカウントの設定
    var page_Num = 0;
    var S = s_Value.editContents;
    var E = e_Value.editContents;
    var sp = parseInt(S);
    var ep = parseInt(E);
    var tp = ep - sp + 1;

    //====================//
    //中綴じと平綴じの面付け//
    //====================//
    if ((tp % 4 == 0) && (bindding < 4)) {
        //中左閉じ
        if (bindding == 0) {
            for (r = 0; r < tp / 2; r++) {
                saddle_Left();
                //配置ページが最後の場合はbreak、それ以外はページ追加
                if (r == tp / 2 - 1) {
                    break;
                } else {
                    app.activeDocument.pages.add();
                    page_Num++;
                }
            }
        }
        //中右閉じ
        else if (bindding == 1) {
            for (r = 0; r < tp / 2; r++) {
                saddle_Right();
                //配置ページが最後の場合はbreak、それ以外はページ追加
                if (r == tp / 2 - 1) {
                    break;
                } else {
                    app.activeDocument.pages.add();
                    page_Num++;
                }
            }
        }

        //平左閉じ
        else if (bindding == 2) {
            for (r = 0; r < tp / 4; r++) {
                side_Left();
                //配置ページが最後の場合はbreak、それ以外はページ追加
                if (r == tp / 4 - 1) {
                    break;
                } else {
                    app.activeDocument.pages.add();
                    page_Num++;
                }
            }
        }

        //平右閉じ
        else if (bindding == 3) {
            for (r = 0; r < tp / 4; r++) {
                side_Right();
                //配置ページが最後の場合はbreak、それ以外はページ追加
                if (r == tp / 4 - 1) {
                    break;
                } else {
                    app.activeDocument.pages.add();
                    page_Num++;
                }
            }
        }
    }
    //====================//
    //　リピートの面付け　　//
    //====================//
    else if (bindding == 4) {
        for (r = 0; r < tp; r++) {
            repeat();
            //配置ページが最後の場合はbreak、それ以外はページ追加
            if (r == tp - 1) {
                break;
            } else {
                app.activeDocument.pages.add();
                page_Num++;
            }
        }
    }

    //==========================//
    //　カット&スタックの面付け　 //
    //==========================//
    else if ((tp % 2 == 0) && (bindding == 5)) {
        for (r = 0; r < tp / 2; r++) {
            cut_stack();
            //配置ページが最後の場合はbreak、それ以外はページ追加
            if (r == tp / 2 - 1) {
                break;
            } else {
                app.activeDocument.pages.add();
                page_Num++;
            }
        }
    } else {
        alert("ページ数を確認して下さい。（PDFで白ページを追加してから再度実行して下さい）");
    }

} else {
    alert("キャンセルされました")
}